﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> cities = new List<string>() { "Plovdiv", "Varna" };
            cities.Add("Sofia");
            cities.Add("Veliko Tarnovo");

            foreach (var city in cities)
            {
                Console.WriteLine(city);
            }

            cities.Sort();

            cities.ForEach(c => { Console.WriteLine(c); });

            Console.WriteLine("Using Where()");
            cities.Where(c => c.StartsWith("V"))
                .ToList()
                .ForEach(c => { Console.WriteLine(c); });
        }
    }
}
