﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionWithClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            List<City> cities = new List<City>();
            cities.Add(new City("Plovdiv", 300000));
            cities.Add(new City("Varna", 200000));
            cities.Add(new City("Sofia", 1200000));
            cities.Add(new City("Veliko Tarnovo", 100000));

            foreach (var city in cities)
            {
                Console.WriteLine(city);
            }

            cities.Sort();

            Console.WriteLine("\nSorted by population");
            cities.ForEach(c => { Console.WriteLine(c); });

            //sorted names alphabetically/lexicographically
            Console.WriteLine("\nNames sorted in alphabetical order");
            List<string> names = cities.Select(c => c.Name)
                .OrderBy(n => n)
                .ToList();
               
            names.ForEach(n => { Console.WriteLine(n); });

            //sorted cities by name descending
            Console.WriteLine("\nCities sorted by name descending");
            List<City> citiesByName = cities.OrderByDescending(c => c.Name).ToList();
            citiesByName.ForEach(c => { Console.WriteLine(c); });

        }
    }
}
