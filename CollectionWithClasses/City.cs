﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionWithClasses
{
    class City : IComparable
    {
        public string Name { get; set; }
        public int Population { get; set; }
        public double Area { get; set; }

        public City(string name, int population)
        {
            Name = name;
            Population = population;
        }

        public override string ToString()
        {
            return $"{Name} [{Population}]";
        }

        public int CompareTo(object obj)
        {
            City other = obj as City;

            if (this.Population > other.Population)
                return 1;
            if (this.Population == other.Population)
                return 0;

            return -1;
        }
    }
}
